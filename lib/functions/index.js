// ----------------------------------------------
// index.js
// ----------------------------------------------
// Exportings all functions.
// Here is where we export all functions created in app.

import messageComplition from "./complition/complition.function.js";
import validPhoneNumber from "./valid/valid.function.js";
import errorMessage from "./error/error.function.js";

export { messageComplition, validPhoneNumber, errorMessage };
