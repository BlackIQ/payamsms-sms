// ----------------------------------------------
// index.js
// ----------------------------------------------
// Exportings all configurations.
// When we create a config, we have to export it here.

import errorsConfig from "./errors/errors.config.js";

export { errorsConfig };
