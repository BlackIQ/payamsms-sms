// ----------------------------------------------
// index.js
// ----------------------------------------------
// Exportings all services.
// Services are for calling API endpoints.

import * as SMS from "./sms/sms.services.js";

export { SMS };
